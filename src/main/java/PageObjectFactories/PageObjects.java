package PageObjectFactories;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class PageObjects {

    public PageObjects(AppiumDriver driver) {
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }

    // Home page
    @AndroidFindBy(id = "com.architactile.legguide_oeac_56:id/imageButtonAbout")
    public AndroidElement home_button;

    @AndroidFindBy(id = "com.architactile.legguide_oeac_56:id/aboutButton")
    public AndroidElement about_button;

    @AndroidFindBy(id = "com.architactile.legguide_oeac_56:id/imageButtonState")
    public AndroidElement state_button;

    // About page
    @AndroidFindBy(id = "com.architactile.legguide_oeac_56:id/textView1")
    public AndroidElement about_text;

    @AndroidFindBy(id = "com.architactile.legguide_oeac_56:id/memberSystemsButton")
    public AndroidElement member_systems_buttons;

    // State page
    @AndroidFindBy(xpath = "//android.widget.Button[@text='Back']")
    public AndroidElement back_button;

    @AndroidFindBy(className = "android.widget.TextView")
    public List<AndroidElement> state_page_buttons_text;

    @AndroidFindBy(id = "com.architactile.legguide_oeac_56:id/stateSenateButton")
    public AndroidElement senate_button;

    // Senate page
    @AndroidFindBy(id = "com.architactile.legguide_oeac_56:id/sectionNameTextView")
    public AndroidElement ok_senate_text;

    @AndroidFindBy(id = "com.architactile.legguide_oeac_56:id/searchEditText")
    public AndroidElement search_box;

    @AndroidFindBy(id = "com.architactile.legguide_oeac_56:id/personCellNameTextView")
    public AndroidElement senator_found;

    // Senator page
    @AndroidFindBy (id = "com.architactile.legguide_oeac_56:id/address1Line1TextView")
    public AndroidElement senator_address1;

    @AndroidFindBy (id = "com.architactile.legguide_oeac_56:id/address1Line2TextView")
    public AndroidElement senator_address2;

    @AndroidFindBy (id = "com.architactile.legguide_oeac_56:id/address1MapButton")
    public AndroidElement map_address_button;

    // Google Maps
    @AndroidFindBy(xpath = "(//android.widget.TextView)[2]")
    public AndroidElement address_text_box;

    // Senate Committees
    @AndroidFindBy(id = "com.architactile.legguide_oeac_56:id/stateSenateCommitteeButton")
    public AndroidElement senate_committees_button;

    @AndroidFindBy(id = "com.architactile.legguide_oeac_56:id/sectionNameTextView")
    public AndroidElement standing_committees;
}

