package navigation;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import resources.Base;
import resources.Log;

public class AppNav extends Base {

    /**
     * @method navigateToStatePage
     * @description Navigates to the app's State page
     * @author Arianna Baxter | 9/02/2019
     */
    public static void navigateToStatePage() {
        Log.debug("Clicking the State button");

        wait.until(ExpectedConditions.elementToBeClickable(and_elem.state_button));
        and_elem.state_button.click();
        wait.until(ExpectedConditions.visibilityOf(and_elem.state_page_buttons_text.get(3)));

        Log.debug("State page opened");
    }

    /**
     * @method navigateToAboutPage
     * @description Navigates to the app's About page
     * @author Arianna Baxter | 9/02/2019
     */
    public static void navigateToAboutPage() {
        Log.debug("Clicking the About button");

        wait.until(ExpectedConditions.elementToBeClickable(and_elem.about_button));
        and_elem.about_button.click();
        wait.until(ExpectedConditions.visibilityOf(and_elem.about_text));

        Log.debug("About page opened");
    }

    /**
     * @method navigateToHomePage
     * @description Navigates to the app's home page
     * @author Arianna Baxter | 9/02/2019
     */
    public static void navigateToHomePage() {
        Log.debug("Clicking the About button");

        wait.until(ExpectedConditions.elementToBeClickable(and_elem.home_button));
        and_elem.home_button.click();
        wait.until(ExpectedConditions.visibilityOf(and_elem.home_button));

        Log.debug("Home page opened");
    }

    /**
     * @method navigateToSenatePage
     * @description Navigates to the app's Senate page
     * @author Arianna Baxter | 9/02/2019
     */
    public static void navigateToSenatePage() {
        Log.debug("Clicking the Senate button");

        wait.until(ExpectedConditions.elementToBeClickable(and_elem.senate_button));
        and_elem.senate_button.click();
        wait.until(ExpectedConditions.visibilityOf(and_elem.ok_senate_text));

        Log.debug("Senate page opened");
    }

    /**
     * @method navigateToSenatorsInfo
     * @description Navigates to the senator's info page
     * @author Arianna Baxter | 11/02/2019
     * @param senator The senator to navigate to.
     */
    public static void navigateToSenatorsInfo(String senator) {
        Log.debug("Opening " + senator + "'s page.");
        wait.until(ExpectedConditions.elementToBeClickable(and_elem.search_box));
        and_elem.search_box.sendKeys(senator);
        wait.until(ExpectedConditions.textToBePresentInElement(and_elem.senator_found, senator));
        and_elem.senator_found.click();
    }

    /**
     * @method navigateToSenateCommitteesPage
     * @description Navigates to the Senate Committees page
     * @author Arianna Baxter | 11/02/2019
     */
    public static void navigateToSenateCommitteesPage() {
        Log.debug("Opening Senate Committees page.");
        wait.until(ExpectedConditions.elementToBeClickable(and_elem.senate_committees_button));
        and_elem.senate_committees_button.click();
        wait.until(ExpectedConditions.textToBePresentInElement(and_elem.standing_committees, " Standing Committee"));
    }

    /**
     * @method navigateToCommittee
     * @description Navigates to the committee page
     * @author Arianna Baxter | 11/02/2019
     * @param committee The committee to navigate to.
     */
    public static void navigateToCommittee(String committee) {
        // Scroll to committee
        scrollToText(committee);

        // Click committee
        wait.until(ExpectedConditions.elementToBeClickable(
                By.xpath("//android.widget.TextView[@text='" + committee + "']")));
        driver.findElement(By.xpath("//android.widget.TextView[@text='" + committee + "']")).click();
    }
}
