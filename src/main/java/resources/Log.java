package resources;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.markuputils.Markup;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class Log {
    public static Logger log = LogManager.getLogger("Logger");
    public static ExtentReports report;
    public static ExtentTest testLog;

    /**
     * method start
     * description Logs message as pass in Extent and Info in log4j
     * author Arianna Baxter | 10/2/2018
     * param message Message to log
     */
    public static void start(String message) {
        testLog.pass(message);
        log.info(message);
    }

    /**
     * method start
     * description Logs message as fatal in Extent and log4j
     * author Arianna Baxter | 10/2/2018
     * param message Message to log
     */
    public static void fatal(String message) {
        testLog.fatal(message);
        log.fatal(message);
    }

    /**
     * method start
     * description Logs message as fail in Extent and error in log4j
     * author Arianna Baxter | 10/2/2018
     * param message Message to log
     */
    public static void fail(String message) {
        testLog.fail(message);
        log.error(message);
    }

    /**
     * method start
     * description Logs message as error in Extent and log4j
     * author Arianna Baxter | 10/2/2018
     * param message Message to log
     */
    public static void error(String message) {
        testLog.error(message);
        log.error(message);
    }

    /**
     * method start
     * description Logs message as warn in Extent and log4j
     * author Arianna Baxter | 10/2/2018
     * param message Message to log
     */
    public static void warning(String message) {
        testLog.warning(message);
        log.warn(message);
    }

    /**
     * method start
     * description Logs message as skip in Extent and Info in log4j
     * author Arianna Baxter | 10/2/2018
     * param message Message to log
     */
    public static void skip(String message) {
        testLog.skip(message);
        log.info(message);
    }

    /**
     * method start
     * description Logs message as pass in Extent and debug in log4j
     * author Arianna Baxter | 10/2/2018
     * param message Message to log
     */
    public static void pass(String message) {
        testLog.pass(message);
        log.debug(message);
    }

    /**
     * method start
     * description Logs message as debug in Extent and log4j
     * author Arianna Baxter | 10/2/2018
     * param message Message to log
     */
    public static void debug(String message) {
        testLog.debug(message);
        log.debug(message);
    }

    /**
     * method start
     * description Logs message as info in Extent and log4j
     * author Arianna Baxter | 10/2/2018
     * param message Message to log
     */
    public static void info(String message) {
        testLog.info(message);
        log.info(message);
    }

    /**
     * method start
     * description Logs message as pass in Extent and Info in log4j
     * author Arianna Baxter | 10/2/2018
     * param message Message to log
     */
    public static void success(String message) {
        testLog.pass(message);
        log.info(message);
    }

    /**
     * method getScreenshot
     * description Takes a screenshot of the browser
     * author Arianna Baxter | 27/08/2018
     * param result - Name of screenshot
     */
    public static String getScreenshot(String result) {
        String screenshotFilePath;
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sd = new SimpleDateFormat("M-d-yyyy_HH.mm.ss");
        String basePath = System.getProperty("user.dir") + "/runData/Reports/Screenshot/";
        String fileName = sd.format(cal.getTime()) + ".png";

        log.debug("Screenshot taken: " + result);

        File src = Base.driver.getScreenshotAs(OutputType.FILE);

        screenshotFilePath = basePath + fileName;
        try {
            FileUtils.copyFile(src, new File(screenshotFilePath));
        } catch (IOException e) {
            log.error("Unable to take Screenshot");
        }

        return fileName;
    }

    /**
     * method setUpExtentTestLogger
     * description Creates the test for logging
     * author Arianna Baxter | 27/08/2018
     * param method - Method used to get name of test
     * param author - Author of the test
     * param category - Which category to assign the test
     */
    public static void setUpExtentTestLogger(Method method, String author, String category) {
        testLog = report.createTest(method.getName());
        testLog.assignAuthor(author);
        testLog.assignCategory(category);
    }

    /**
     * method attachImageToTestLog
     * description - Attaches images inline with test steps
     * author Arianna Baxter | 27/08/2018
     * param message - Message above text
     * param fileName - name of screenshot
     */
    public static void attachImageToTestLog(String message, String fileName) {
        try {
            testLog.fail(message, MediaEntityBuilder.createScreenCaptureFromPath("Screenshot/" + fileName).build());
        } catch (IOException e1) {
            testLog.warning("Unable to attach image");
        }
    }

    /**
     * method logErrorAndStackTrace
     * description logs the error and stack trace of the throwable
     * author Arianna Baxter | 9/12/2018
     * param throwable throwable that was thrown
     */
    public static void logErrorAndStackTrace(Throwable throwable) {
        Markup m;
        StringBuilder stackTrace = new StringBuilder();
        String error = throwable.toString();

        for (int i = 0; i < throwable.getStackTrace().length && i < 20; i++) {
            stackTrace.append(throwable.getStackTrace()[i]).append("\n");
        }

        log.error(error);
        m = MarkupHelper.createCodeBlock(error);
        testLog.error(m);
        m = MarkupHelper.createCodeBlock(stackTrace.toString());
        testLog.debug(m);
    }
}
