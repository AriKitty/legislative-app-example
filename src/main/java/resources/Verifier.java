package resources;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;

import java.util.ArrayList;
import java.util.List;

import static java.lang.Integer.parseInt;

public class Verifier {
    private static Logger logger = LogManager.getLogger("Verifier");
    private List<String> exceptedValues = new ArrayList<>();
    private boolean pass = true;
    private boolean printStackTrace = false;

    Verifier() {
    }

    /**
     * method verifyEquals
     * description Verifies the data matches
     * author Arianna Baxter | 9/26/2018
     * param expected Expected value
     * param actual Value to check
     * return Verifier
     */
    public Verifier equals(int expected, int actual) {
        try {
            Assert.assertEquals(actual, expected);
        } catch (AssertionError e) {
            setExceptedValues(expected, actual);
            if (this.printStackTrace) Log.logErrorAndStackTrace(e);
            this.pass = false;
        }

        return this;
    }

    /**
     * method verifyEquals
     * description Verifies the data matches
     * author Arianna Baxter | 9/26/2018
     * param expected Expected value
     * param actual Value to check
     * return Verifier
     */
    public Verifier equals(int expected, String actual) {
        try {
            Assert.assertEquals(parseInt(actual), expected);
        } catch (AssertionError e) {
            this.setExceptedValues(expected, actual);
            if (this.printStackTrace) Log.logErrorAndStackTrace(e);
            this.pass = false;
        }

        return this;
    }

    /**
     * method verifyEquals
     * description Verifies the data matches
     * author Arianna Baxter | 9/26/2018
     * param expected Expected value
     * param actual Value to check
     * return Verifier
     */
    public Verifier equals(String expected, int actual) {
        try {
            Assert.assertEquals(actual, parseInt(expected));
        } catch (AssertionError e) {
            this.setExceptedValues(expected, actual);
            if (this.printStackTrace) Log.logErrorAndStackTrace(e);
            this.pass = false;
        }

        return this;
    }

    /**
     * method verifyEquals
     * description Verifies the data matches
     * author Arianna Baxter | 9/26/2018
     * param expected Expected value
     * param actual Value to check
     * return Verifier
     */
    public Verifier equals(String expected, String actual) {
        try {
            Assert.assertEquals(actual, expected);
        } catch (AssertionError e) {
            this.setExceptedValues(expected, actual);
            if (this.printStackTrace) Log.logErrorAndStackTrace(e);
            this.pass = false;
        }

        return this;
    }

    /**
     * method verifyNotEquals
     * description Verifies the data does not match
     * author Arianna Baxter | 9/26/2018
     * param expected Expected value
     * param actual Value to check
     * return Verifier
     */
    public Verifier notEquals(int expected, int actual) {
        try {
            Assert.assertNotEquals(actual, expected);
        } catch (AssertionError e) {
            setExceptedValues(expected, actual);
            if (this.printStackTrace) Log.logErrorAndStackTrace(e);
            this.pass = false;
        }

        return this;
    }

    /**
     * method verifyNotEquals
     * description Verifies the data does not match
     * author Arianna Baxter | 9/26/2018
     * param expected Expected value
     * param actual Value to check
     * return Verifier
     */
    public Verifier notEquals(int expected, String  actual) {
        try {
            Assert.assertNotEquals(parseInt(actual), expected);
        } catch (AssertionError e) {
            this.setExceptedValues(expected, actual);
            if (this.printStackTrace) Log.logErrorAndStackTrace(e);
            this.pass = false;
        }

        return this;
    }

    /**
     * method verifyNotEquals
     * description Verifies the data does not match
     * author Arianna Baxter | 9/26/2018
     * param expected Expected value
     * param actual Value to check
     * return Verifier
     */
    public Verifier notEquals(String expected, int  actual) {
        try {
            Assert.assertNotEquals(actual, parseInt(expected));
        } catch (AssertionError e) {
            this.setExceptedValues(expected, actual);
            if (this.printStackTrace) Log.logErrorAndStackTrace(e);
            this.pass = false;
        }

        return this;
    }

    /**
     * method verifyNotEquals
     * description Verifies the data does not match
     * author Arianna Baxter | 9/26/2018
     * param expected Expected value
     * param actual Value to check
     * return Verifier
     */
    public Verifier notEquals(String expected, String actual) {
        try {
            Assert.assertNotEquals(actual, expected);
        } catch (AssertionError e) {
            this.setExceptedValues(expected, actual);
            if (this.printStackTrace) Log.logErrorAndStackTrace(e);
            this.pass = false;
        }

        return this;
    }

    /**
     * method verifyTrue
     * description Verifies the data is true
     * author Arianna Baxter | 9/26/2018
     * param value Value to check
     * return Verifier
     */
    public Verifier verifyTrue(boolean value) {
        try {
            Assert.assertTrue(value);
        } catch (AssertionError e) {
            this.setExceptedValues("true", String.valueOf(value));
            if (this.printStackTrace) Log.logErrorAndStackTrace(e);
            this.pass = false;
        }

        return this;
    }

    /**
     * method verifyFalse
     * description Verifies the data is false
     * author Arianna Baxter | 9/26/2018
     * param value Value to check
     * return Verifier
     */
    public Verifier verifyFalse(boolean value) {
        try {
            Assert.assertFalse(value);
        } catch (AssertionError e) {
            this.setExceptedValues("false", String.valueOf(value));
            if (this.printStackTrace) Log.logErrorAndStackTrace(e);
            this.pass = false;
        }

        return this;
    }

    /**
     * method verifyEquals
     * description Verifies the data matches
     * author Arianna Baxter | 9/27/2018
     * param error Error to log
     * return Verifier
     */
    public Verifier onFailureGetScreenshot(String error) {
        if (!this.pass) {
            error = getExceptedValues(error);
            logger.error(error);
            String file = Log.getScreenshot(error);
            Log.attachImageToTestLog(error, file);
        }

        return this;
    }

    /**
     * method verifyEquals
     * description Verifies the data matches
     * author Arianna Baxter | 9/27/2018
     * param error Error to log
     * return Verifier
     */
    public Verifier onFailure(String error) {
        if (!this.pass)  {
            error = getExceptedValues(error);
            Log.fail(error);
        }
        return this;
    }

    /**
     * method verifyEquals
     * description Verifies the data matches
     * author Arianna Baxter | 9/26/2018
     * param message Message to log
     * return Verifier
     */
    public Verifier onSuccess(String message) {
        if (this.pass) Log.pass(message);
        return this;
    }

    /**
     * method setExceptedValues
     * description Adds the expected values to the list of failures
     * author Arianna Baxter | 10/25/2018
     * param expected Expected value
     * param actual Value shown
     */
    private void setExceptedValues(int expected, int actual) {
        this.exceptedValues.add(String.valueOf(expected));
        this.exceptedValues.add(String.valueOf(actual));
    }

    /**
     * method setExceptedValues
     * description Adds the expected values to the list of failures
     * author Arianna Baxter | 10/25/2018
     * param expected Expected value
     * param actual Value shown
     */
    private void setExceptedValues(String expected, String actual) {
        this.exceptedValues.add(expected);
        this.exceptedValues.add(actual);
    }

    /**
     * method setExceptedValues
     * description Adds the expected values to the list of failures
     * author Arianna Baxter | 10/25/2018
     * param expected Expected value
     * param actual Value shown
     */
    private void setExceptedValues(String expected, int actual) {
        this.exceptedValues.add(expected);
        this.exceptedValues.add(String.valueOf(actual));
    }

    /**
     * method setExceptedValues
     * description Adds the expected values to the list of failures
     * author Arianna Baxter | 10/25/2018
     * param expected Expected value
     * param actual Value shown
     */
    private void setExceptedValues(int expected, String actual) {
        this.exceptedValues.add(String.valueOf(expected));
        this.exceptedValues.add(actual);
    }

    /**
     * method getExceptedValues
     * description Addes Expected and Actual statements to errors
     * author Arianna Baxter | 10/25/2018
     * param message Message to modify
     * return String Message with expectations added in
     */
    private String getExceptedValues(String message) {

        // Add all the checked expectations
        for (int i = 0; i < this.exceptedValues.size(); i++) {
            message += "\nExpected: '" + this.exceptedValues.get(i) + "' actual: '" + this.exceptedValues.get(++i) + "'";
        }

        clearExceptedValues();
        return message;
    }

    /**
     * method clearExceptedValues
     * description Clears the exceptedValues
     * author Arianna Baxter | 10/25/2018
     */
    private void clearExceptedValues() {
        this.exceptedValues.clear();
    }

    /**
     * method reset
     * description Resets pass back to true
     * author Arianna Baxter | 9/27/2018
     */
    private void reset() {
        this.pass = true;
        this.printStackTrace = false;
    }

    /**
     * method getPass
     * description Gets numerical representation of test success
     * author Arianna Baxter | 9/27/2018
     * return int 1 for success, 0 for failure
     */
    public int getPass() {
        int returnValue = this.pass ? 1 : 0;
        reset();
        return returnValue;
    }

    /**
     * method printStackTrace
     * description Enables the printing of stack trace on failure. Must be called before verify.
     * author Arianna Baxter | 9/27/2018
     * return Verifier
     */
    public Verifier printStackTrace() {
        this.printStackTrace = true;
        return this;
    }
}
