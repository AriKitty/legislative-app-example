package listeners;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.markuputils.Markup;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.ChartLocation;
import com.aventstack.extentreports.reporter.configuration.Protocol;
import com.aventstack.extentreports.reporter.configuration.Theme;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;
import resources.Log;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class Reporter implements ITestListener {

    private static ExtentReports reports;

    @Override
    public void onTestStart(ITestResult result) {

    }

    @Override
    public void onTestSuccess(ITestResult result) {

    }

    @Override
    public void onTestFailure(ITestResult result) {
        Log.fail(result.getName() + " failed");
        StringBuilder stackTrace = new StringBuilder();
        String error = result.getThrowable().toString();
        Log.fail(error);

        // Print stack tract if not assertion Error
        if (!error.contains("AssertionError")) {
            for (int i = 0; i < result.getThrowable().getStackTrace().length && i < 30; i++) {
                stackTrace.append(result.getThrowable().getStackTrace()[i]).append("\n");
            }
            Markup m = MarkupHelper.createCodeBlock(stackTrace.toString());
            Log.testLog.debug(m);
        }

        // Attach screenshot in test steps
        String fileName = Log.getScreenshot(result.getName());
        Log.attachImageToTestLog("Test Failed", fileName);
    }

    @Override
    public void onTestSkipped(ITestResult result) {
        Log.testLog = Log.report.createTest(result.getName());

        // Get skipped Test's name
        String skippedTest = result.getTestClass().getName();
        if (skippedTest.contains("tests.")) {
            skippedTest = skippedTest.substring(6);
        }
        Log.testLog.assignCategory(skippedTest);
        Log.skip(result.getName() + " skipped");
        Log.debug(result.getThrowable().toString());

    }

    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult result) {

    }

    @Override
    public void onStart(ITestContext result) {

        // Create file
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sd = new SimpleDateFormat("M-d-yyyy HH.mm.ss");
        String basePath = System.getProperty("user.dir") + "/runData/Reports/";
        String profileName = System.getProperty("profileName");
        String fileName = "AppReport_" + profileName + " " + sd.format(cal.getTime()) + ".html";
        File reportFile = new File(basePath + fileName);
        ExtentHtmlReporter htmlReporter = new ExtentHtmlReporter(reportFile);
        reports = new ExtentReports();

        // Set system info
        reports.setSystemInfo("Environment", System.getProperty("environment"));
        reports.setSystemInfo("Browser", System.getProperty("browser"));
        reports.setSystemInfo("OS", System.getProperty("os"));
        reports.setSystemInfo("OS Version", System.getProperty("osVersion"));

        // Configure report
        htmlReporter.config().setEncoding("UTF-8");
        htmlReporter.config().setChartVisibilityOnOpen(true);
        htmlReporter.config().setTestViewChartLocation(ChartLocation.BOTTOM);
        htmlReporter.config().setTheme(Theme.DARK);
        htmlReporter.config().setDocumentTitle("Gradebook Automation Report");
        htmlReporter.config().setProtocol(Protocol.HTTPS);
        reports.attachReporter(htmlReporter);
        Log.report = reports;
    }

    @Override
    public void onFinish(ITestContext result) {
        reports.flush();
    }

}
