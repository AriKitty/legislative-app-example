package tests;

import navigation.AppNav;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import resources.Log;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;

public class AppFunctionality extends SuiteWide  {

    /**
     * method setupTestLogging
     * description Creates the Extent Test Logger and sets pass = 0
     * author Arianna Baxter | 9/12/2018
     */
    @BeforeMethod
    public void setupTestLogging(Method method) {
        Log.setUpExtentTestLogger(method, "Arianna Baxter", "App Functionality");
        pass = 0;
    }

    /**
     * method verifyCommitteeMembers
     * description Verifies that the Members of the Public Safety & Judiciary Appropriations Committee are listed correctly
     * author Arianna Baxter | 2/10/2019
     */
    @Test (priority = 3)
    public void verifyCommitteeMembers() {
        test_name = "Verify Public Safety & Judiciary Appropriations Committee Members";
        steps = 0;
        Log.start(test_name);

        // Navigate to the senate page
        String committee = "Public Safety & Judiciary";
        AppNav.navigateToStatePage();
        AppNav.navigateToSenateCommitteesPage();
        AppNav.navigateToCommittee(committee);

        // Verify the committee members
        List<String> committee_members = Arrays.asList("David Holt", "Anthony Sykes", "Larry Boggs", "Michael Brooks",
                "Kay Floyd", "Jack Fry", "Roland Pederson");
        steps = committee_members.size();
        for (String committee_member : committee_members) {
            scrollToText(committee_member);
            pass += verify.equals(committee_member, driver.findElement(By.xpath("//android.widget.TextView[@text='" + committee_member + "']")).getText())
                    .onFailureGetScreenshot("Incorrect senator name: "
                            + driver.findElement(By.xpath("//android.widget.TextView[@text='" + committee_member + "']")).getText()
                            + " Expected: " + committee_member)
                    .onSuccess(committee_member + " in " + committee + " committee.")
                    .getPass();
        }

        // Return to original senate screen
        wait.until(ExpectedConditions.elementToBeClickable(and_elem.back_button));
        and_elem.back_button.click();
        wait.until(ExpectedConditions.elementToBeClickable(and_elem.back_button));
        and_elem.back_button.click();

        endTest(steps, pass, test_name);
    }

    /**
     * method verifyKayFloydsAddressInMaps
     * description Verifies that Kay Floyd's info is correct in the app and opens the correct address in Google Maps
     * author Arianna Baxter | 2/10/2019
     */
    @Test (priority = 4)
    public void verifyKayFloydsAddressInMaps() {
        test_name = "Verify Kay Floyd's Address in Google Maps";
        steps = 2;
        Log.start(test_name);

        // Navigate to the senator's page
        String senator = "Kay Floyd";
        AppNav.navigateToStatePage();
        AppNav.navigateToSenatePage();
        AppNav.navigateToSenatorsInfo(senator);

        // Verify the address in app
        String address1 = "2300 N. Lincoln Blvd., 520A";
        String address2 = "Oklahoma City, OK 73105";

        pass += verify.equals(address1, and_elem.senator_address1.getText())
                .equals(address2, and_elem.senator_address2.getText())
                .onFailureGetScreenshot(senator + "'s address is incorrect")
                .onSuccess(senator + "'s address is correct")
                .getPass();

        // Open address in Google Maps and verify correct address
        String full_address = "2300 N. Lincoln Blvd., 520A, Oklahoma City, OK 73105";
        wait.until(ExpectedConditions.elementToBeClickable(and_elem.map_address_button));
        and_elem.map_address_button.click();
        wait.until(ExpectedConditions.visibilityOf(and_elem.address_text_box));

        pass += verify.equals(full_address, and_elem.address_text_box.getText())
                .onFailureGetScreenshot("Address is not correct in Maps")
                .onSuccess("Address is correct in Maps")
                .getPass();

        endTest(steps, pass, test_name);
    }
}
