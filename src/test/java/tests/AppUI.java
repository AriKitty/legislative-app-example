package tests;

import navigation.AppNav;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import resources.Log;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;

public class AppUI extends SuiteWide  {

    /**
     * method setupTestLogging
     * description Creates the Extent Test Logger and sets pass = 0
     * author Arianna Baxter | 9/12/2018
     */
    @BeforeMethod
    public void setupTestLogging(Method method) {
        Log.setUpExtentTestLogger(method, "Arianna Baxter", "App UI");
        pass = 0;
    }

    /**
     * method verifyAboutUI
     * description Verifies UI on the About page
     * author Arianna Baxter | 2/10/2019
     */
    @Test (priority = 1)
    public void verifyAboutUI() {
        test_name = "Verify About Page UI";
        steps = 1;
        Log.start(test_name);

        AppNav.navigateToAboutPage();
        String about_text = "The Oklahoma Association of Electric Cooperatives (OAEC) " +
                "is a statewide association voluntarily supported by local electric distribution " +
                "and generation/transmission cooperatives. OAEC is comprised of 30 member systems, " +
                "28 in Oklahoma and two Arkansas systems with a substantial portion of their membership " +
                "residing in Oklahoma.";

        pass += verify.equals(about_text, and_elem.about_text.getText())
                .verifyTrue(and_elem.member_systems_buttons.getAttribute("clickable").equals("true"))
                .onFailureGetScreenshot("About UI is incorrect")
                .onSuccess("About UI is correct")
                .getPass();

        AppNav.navigateToHomePage();
        endTest(steps, pass, test_name);
    }

    /**
     * method verifyStatePageUI
     * description Verifies each of the button texts on the State page
     * author Arianna Baxter | 2/10/2019
     */
    @Test (priority = 2)
    public void verifyStatePageUI() {
        test_name = "Verify State Page UI";
        List<String> state_buttons_text = Arrays.asList("Statewide Officials", "Judiciary", "Senate Leadership", "Senate",
                "Senate Committees", "House Leadership", "House", "House Committees", "All");
        steps = state_buttons_text.size();
        Log.start(test_name);

        AppNav.navigateToStatePage();

        // Check each button's text
        for (int i = 0; i < state_buttons_text.size(); i++) {
            pass += verify.equals(state_buttons_text.get(i), and_elem.state_page_buttons_text.get(i).getText())
                    .onFailureGetScreenshot(state_buttons_text.get(i) + " text is incorrect")
                    .onSuccess(state_buttons_text.get(i) + " text is correct")
                    .getPass();
        }

        endTest(steps, pass, test_name);
    }
}
