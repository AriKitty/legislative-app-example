package tests;

import navigation.AppNav;
import org.openqa.selenium.By;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import resources.Log;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;

public class Bugtest extends SuiteWide {

    /**
     * method setupTestLogging
     * description Creates the Extent Test Logger and sets pass = 0
     * author Arianna Baxter | 9/12/2018
     */
    @BeforeMethod
    public void setupTestLogging(Method method) {
        Log.setUpExtentTestLogger(method, "Arianna Baxter", "Bug Testing");
        pass = 0;
    }



}
