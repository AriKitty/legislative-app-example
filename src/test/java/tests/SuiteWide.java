package tests;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import PageObjectFactories.PageObjects;
import com.aventstack.extentreports.markuputils.Markup;
import io.appium.java_client.android.AndroidElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.annotations.*;
import resources.Base;
import resources.Log;

public class SuiteWide extends Base {
    private Calendar cal;
    private SimpleDateFormat sd = new SimpleDateFormat("MM-dd-yyyy hh:mm:ss");

    protected Markup m;
    String test_name;
    int pass;
    int steps;

    /**
     * method openBrowser
     * description Initialized driver and opens browser
     * author Arianna Baxter | 18/01/2018
     */
    @BeforeSuite
    public void initialize() {
        cal = Calendar.getInstance();
        Log.log.info(
                "\n***************************************\n"
                        + "*          Beginning of log           *\n"
                        + "*         "
                        + sd.format(cal.getTime())
                        + "         *\n"
                        + "***************************************\n");
        initializeEmulator();
        setupTestVariables();
    }

    /**
     * method closeBrowser
     * description Closes the browser
     * author Arianna Baxter | 18/01/2018
     */
    @AfterSuite
    public void closeBrowser() {
        cal = Calendar.getInstance();
//        driver.quit();
        Log.log.info(
                "\n***************************************\n"
                        + "*             End of log              *\n"
                        + "*         "
                        + sd.format(cal.getTime())
                        + "         *\n"
                        + "***************************************\n");
    }

    /**
     * method setupTestVariables
     * description Sets up variables needed throughout the suite
     * author Arianna Baxter | 11/8/2018
     */
    private void setupTestVariables() {
        and_elem = new PageObjects(driver);
    }

    /**
     * method endTest
     * description Determines pass or failure of test and prints results to logs
     * author Arianna Baxter | 10/8/2018
     * param steps Number of steps in the test
     * param pass Number of steps that passed
     * param testName Name of the test
     */
    void endTest(int steps, int pass, String testName) {
        int failedSteps = steps - pass;

        if (failedSteps != 0) {
            Assert.fail("Test failed: " + testName + ". Steps failed: " + failedSteps);
        } else
            Log.success("Test passed: " + testName);

    }

    /**
     * param element desired element
     * return boolean - load success
     * method waitForElementToLoad
     * description Waits for the desired element to load
     * author Arianna Baxter | 11/13/2018
     */
    boolean waitForElementToLoad(AndroidElement element) {
        int tries = 100;
        for (int i = 0; i < tries; i++) {
            try {
                wait.until(ExpectedConditions.visibilityOf(element));
                return true;
            } catch (NullPointerException e) {
                // ignored
            }
        }
        return false;
    }

}