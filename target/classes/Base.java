package resources;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.time.Duration;
import java.util.concurrent.TimeUnit;

import PageObjectFactories.PageObjects;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import org.openqa.selenium.*;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;

import io.appium.java_client.remote.MobileCapabilityType;
import org.testng.Assert;

public class Base {
    private static final int SHORT_TIMEOUT = 5;
    private static final int MEDIUM_TIMEOUT = 30;
    private static final String APP_LOCATION = "ImportantFiles/Legislative_App.apk";
    protected static Wait<AndroidDriver<AndroidElement>> wait;
    protected static Verifier verify = new Verifier();
    protected static AndroidDriver<AndroidElement> driver;
    protected static PageObjects and_elem;
    protected static TouchAction t;

    /**
     * method initializeDriver
     * description initializes the driver
     * author Arianna Baxter | 27/08/2018
     */
    protected void initializeEmulator() {

        // Select the file
        File fs = new File(APP_LOCATION);

        // Set the capabilities
        DesiredCapabilities cap = new DesiredCapabilities();
        cap.setCapability(MobileCapabilityType.DEVICE_NAME, "AriDemo");
        cap.setCapability("autoGrantPermissions", true);
        cap.setCapability("clearSystemFiles", true);
        cap.setCapability(MobileCapabilityType.APP, fs.getAbsolutePath());

        // Open Android driver
        try {
            driver = new AndroidDriver<>(new URL("http://0.0.0.0:4723/wd/hub"), cap);
        } catch (MalformedURLException e) {
            Log.logErrorAndStackTrace(e);
            Assert.fail("Unable to connect to Appium");
        }
        t = new TouchAction(driver);

        // Create the waits
        driver.manage().timeouts().implicitlyWait(SHORT_TIMEOUT, TimeUnit.SECONDS);
        wait = new FluentWait<>(driver)
                        .withTimeout(Duration.ofSeconds(MEDIUM_TIMEOUT))
                        .ignoring(NoSuchElementException.class)
                        .ignoring(StaleElementReferenceException.class);
    }

    public static void scrollToText(String text) {
        driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true).instance(0))." +
                "scrollIntoView(new UiSelector().textContains(\""+text+"\").instance(0))");
    }
}
